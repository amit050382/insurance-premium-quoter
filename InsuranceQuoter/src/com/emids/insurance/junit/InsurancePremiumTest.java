package com.emids.insurance.junit;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.insurance.quote.test.AgeBasedPremiumCalculator;
import com.insurance.quote.test.BasicPremiumCalculator;
import com.insurance.quote.test.Customer;
import com.insurance.quote.test.Gender;
import com.insurance.quote.test.GenderBasedPremiumCalculator;
import com.insurance.quote.test.Habits;
import com.insurance.quote.test.HabitsBasedPremiumCalculator;
import com.insurance.quote.test.HealthCondition;
import com.insurance.quote.test.HealthConditionBasedPremiumCalculator;
import com.insurance.quote.test.PremiumCalculator;

public class InsurancePremiumTest {
	
	private static Customer cust = new Customer();

	@Before
	public void setUp() throws Exception {
		//Customer cust = new Customer();
		cust.setName("Amit Kumar");
		cust.setAge(35);
		cust.setGender(Gender.MALE.toString());
		Map<String, Boolean> healthCond = new HashMap<>();
		healthCond.put(HealthCondition.HYPERTENSION.toString(), false);
		healthCond.put(HealthCondition.BLOOD_PRESSURE.toString(), false);
		healthCond.put(HealthCondition.BLOOD_SUGAR.toString(), false);
		healthCond.put(HealthCondition.OVERWEIGHT.toString(), false);
		
		cust.setHealthStatus(healthCond);
		
		Map<String, Boolean> habits = new HashMap<>();
		habits.put(Habits.BAD_HABIT_SMOKING.toString(), false);
		habits.put(Habits.BAD_HABIT_ALCOHOL.toString(), false);
		habits.put(Habits.GOOD_HABIT_DAILY_EXERCISE.toString(), false);
		habits.put(Habits.BAD_HABIT_DRUGS.toString(), false);
		
		cust.setHabits(habits);
	}

	@Test
	public void testBasicPremiumForMaleBelow18() {
		cust.setAge(17);
		PremiumCalculator premiumCalc = new HabitsBasedPremiumCalculator(new HealthConditionBasedPremiumCalculator(
				new AgeBasedPremiumCalculator(new GenderBasedPremiumCalculator(new BasicPremiumCalculator()))));
		Assert.assertEquals(5100, premiumCalc.calculatePremium(cust).getPremium(), 2);
	}

	@Test
	public void testBasicPremiumForFemaleBelow18() {
		cust.setGender(Gender.FEMALE.toString());
		cust.setAge(17);
		PremiumCalculator premiumCalc = new HabitsBasedPremiumCalculator(new HealthConditionBasedPremiumCalculator(
				new AgeBasedPremiumCalculator(new GenderBasedPremiumCalculator(new BasicPremiumCalculator()))));
		Assert.assertEquals(5000, premiumCalc.calculatePremium(cust).getPremium(), 2);
	}
	
	@Test
	public void testBasicPremiumForOtherBelow18() {
		cust.setGender(Gender.OTHER.toString());
		cust.setAge(17);
		PremiumCalculator premiumCalc = new HabitsBasedPremiumCalculator(new HealthConditionBasedPremiumCalculator(
				new AgeBasedPremiumCalculator(new GenderBasedPremiumCalculator(new BasicPremiumCalculator()))));
		Assert.assertEquals(5000, premiumCalc.calculatePremium(cust).getPremium(), 2);
	}
	
	@Test
	public void testBasicPremiumForMaleAbove18() {
		PremiumCalculator premiumCalc = new HabitsBasedPremiumCalculator(new HealthConditionBasedPremiumCalculator(
				new AgeBasedPremiumCalculator(new GenderBasedPremiumCalculator(new BasicPremiumCalculator()))));
		Assert.assertEquals(5610, premiumCalc.calculatePremium(cust).getPremium(), 2);
	}
	
	@Test
	public void testBasicPremiumForFemaleAbove18() {
		cust.setGender(Gender.FEMALE.toString());
		PremiumCalculator premiumCalc = new HabitsBasedPremiumCalculator(new HealthConditionBasedPremiumCalculator(
				new AgeBasedPremiumCalculator(new GenderBasedPremiumCalculator(new BasicPremiumCalculator()))));
		Assert.assertEquals(5500, premiumCalc.calculatePremium(cust).getPremium(), 2);
	}
	
	@Test
	public void testBasicPremiumForOtherAbove18() {
		cust.setGender(Gender.OTHER.toString());
		PremiumCalculator premiumCalc = new HabitsBasedPremiumCalculator(new HealthConditionBasedPremiumCalculator(
				new AgeBasedPremiumCalculator(new GenderBasedPremiumCalculator(new BasicPremiumCalculator()))));
		Assert.assertEquals(5500, premiumCalc.calculatePremium(cust).getPremium(), 2);
	}
	
	@Test
	public void testBasicPremiumForMaleAbove18With1HealthCondition() {
		Map<String, Boolean> healthCond = new HashMap<>();
		healthCond.put(HealthCondition.HYPERTENSION.toString(), true);
		healthCond.put(HealthCondition.BLOOD_PRESSURE.toString(), false);
		healthCond.put(HealthCondition.BLOOD_SUGAR.toString(), false);
		healthCond.put(HealthCondition.OVERWEIGHT.toString(), false);
		cust.setHealthStatus(healthCond);
		PremiumCalculator premiumCalc = new HabitsBasedPremiumCalculator(new HealthConditionBasedPremiumCalculator(
				new AgeBasedPremiumCalculator(new GenderBasedPremiumCalculator(new BasicPremiumCalculator()))));
		Assert.assertEquals(5666.10, premiumCalc.calculatePremium(cust).getPremium(), 2);
	}
	
	@Test
	public void testBasicPremiumForMaleAbove18With2HealthCondition() {
		Map<String, Boolean> healthCond = new HashMap<>();
		healthCond.put(HealthCondition.HYPERTENSION.toString(), true);
		healthCond.put(HealthCondition.BLOOD_PRESSURE.toString(), true);
		healthCond.put(HealthCondition.BLOOD_SUGAR.toString(), false);
		healthCond.put(HealthCondition.OVERWEIGHT.toString(), false);
		cust.setHealthStatus(healthCond);
		PremiumCalculator premiumCalc = new HabitsBasedPremiumCalculator(new HealthConditionBasedPremiumCalculator(
				new AgeBasedPremiumCalculator(new GenderBasedPremiumCalculator(new BasicPremiumCalculator()))));
		Assert.assertEquals(5722.76, premiumCalc.calculatePremium(cust).getPremium(), 2);
	}
	
	@Test
	public void testBasicPremiumForMaleAbove18With3HealthCondition() {
		Map<String, Boolean> healthCond = new HashMap<>();
		healthCond.put(HealthCondition.HYPERTENSION.toString(), true);
		healthCond.put(HealthCondition.BLOOD_PRESSURE.toString(), true);
		healthCond.put(HealthCondition.BLOOD_SUGAR.toString(), true);
		healthCond.put(HealthCondition.OVERWEIGHT.toString(), false);
		cust.setHealthStatus(healthCond);
		PremiumCalculator premiumCalc = new HabitsBasedPremiumCalculator(new HealthConditionBasedPremiumCalculator(
				new AgeBasedPremiumCalculator(new GenderBasedPremiumCalculator(new BasicPremiumCalculator()))));
		Assert.assertEquals(5779.98, premiumCalc.calculatePremium(cust).getPremium(), 2);
	}
	
	@Test
	public void testBasicPremiumForMaleAbove18With4HealthCondition() {
		Map<String, Boolean> healthCond = new HashMap<>();
		healthCond.put(HealthCondition.HYPERTENSION.toString(), true);
		healthCond.put(HealthCondition.BLOOD_PRESSURE.toString(), true);
		healthCond.put(HealthCondition.BLOOD_SUGAR.toString(), true);
		healthCond.put(HealthCondition.OVERWEIGHT.toString(), true);
		cust.setHealthStatus(healthCond);
		PremiumCalculator premiumCalc = new HabitsBasedPremiumCalculator(new HealthConditionBasedPremiumCalculator(
				new AgeBasedPremiumCalculator(new GenderBasedPremiumCalculator(new BasicPremiumCalculator()))));
		Assert.assertEquals(5837.78, premiumCalc.calculatePremium(cust).getPremium(), 2);
	}
	
	@Test
	public void testBasicPremiumForMaleAbove18With1BadHabit() {
		Map<String, Boolean> habits = new HashMap<>();
		habits.put(Habits.BAD_HABIT_ALCOHOL.toString(), true);
		habits.put(Habits.BAD_HABIT_DRUGS.toString(), false);
		habits.put(Habits.BAD_HABIT_SMOKING.toString(), false);
		habits.put(Habits.GOOD_HABIT_DAILY_EXERCISE.toString(), false);
		cust.setHabits(habits);
		PremiumCalculator premiumCalc = new HabitsBasedPremiumCalculator(new HealthConditionBasedPremiumCalculator(
				new AgeBasedPremiumCalculator(new GenderBasedPremiumCalculator(new BasicPremiumCalculator()))));
		Assert.assertEquals(5778.3, premiumCalc.calculatePremium(cust).getPremium(), 2);
	}
	
	@Test
	public void testBasicPremiumForMaleAbove18With2BadHabit() {
		Map<String, Boolean> habits = new HashMap<>();
		habits.put(Habits.BAD_HABIT_ALCOHOL.toString(), true);
		habits.put(Habits.BAD_HABIT_DRUGS.toString(), true);
		habits.put(Habits.BAD_HABIT_SMOKING.toString(), false);
		habits.put(Habits.GOOD_HABIT_DAILY_EXERCISE.toString(), false);
		cust.setHabits(habits);
		PremiumCalculator premiumCalc = new HabitsBasedPremiumCalculator(new HealthConditionBasedPremiumCalculator(
				new AgeBasedPremiumCalculator(new GenderBasedPremiumCalculator(new BasicPremiumCalculator()))));
		Assert.assertEquals(5951.64, premiumCalc.calculatePremium(cust).getPremium(), 2);
	}
	
	@Test
	public void testBasicPremiumForMaleAbove18With3BadHabit() {
		Map<String, Boolean> habits = new HashMap<>();
		habits.put(Habits.BAD_HABIT_ALCOHOL.toString(), true);
		habits.put(Habits.BAD_HABIT_DRUGS.toString(), true);
		habits.put(Habits.BAD_HABIT_SMOKING.toString(), true);
		habits.put(Habits.GOOD_HABIT_DAILY_EXERCISE.toString(), false);
		cust.setHabits(habits);
		PremiumCalculator premiumCalc = new HabitsBasedPremiumCalculator(new HealthConditionBasedPremiumCalculator(
				new AgeBasedPremiumCalculator(new GenderBasedPremiumCalculator(new BasicPremiumCalculator()))));
		Assert.assertEquals(6130.19, premiumCalc.calculatePremium(cust).getPremium(), 2);
	}
	
	@Test
	public void testBasicPremiumForMaleAbove18With3BadHabit1GoodHabit() {
		Map<String, Boolean> habits = new HashMap<>();
		habits.put(Habits.BAD_HABIT_ALCOHOL.toString(), true);
		habits.put(Habits.BAD_HABIT_DRUGS.toString(), true);
		habits.put(Habits.BAD_HABIT_SMOKING.toString(), true);
		habits.put(Habits.GOOD_HABIT_DAILY_EXERCISE.toString(), true);
		cust.setHabits(habits);
		PremiumCalculator premiumCalc = new HabitsBasedPremiumCalculator(new HealthConditionBasedPremiumCalculator(
				new AgeBasedPremiumCalculator(new GenderBasedPremiumCalculator(new BasicPremiumCalculator()))));
		Assert.assertEquals(5946.29, premiumCalc.calculatePremium(cust).getPremium(), 2);
	}
	
	@Test
	public void testBasicPremiumForMaleAbove18With1GoodHabit() {
		Map<String, Boolean> habits = new HashMap<>();
		habits.put(Habits.BAD_HABIT_ALCOHOL.toString(), false);
		habits.put(Habits.BAD_HABIT_DRUGS.toString(), false);
		habits.put(Habits.BAD_HABIT_SMOKING.toString(), false);
		habits.put(Habits.GOOD_HABIT_DAILY_EXERCISE.toString(), true);
		cust.setHabits(habits);
		PremiumCalculator premiumCalc = new HabitsBasedPremiumCalculator(new HealthConditionBasedPremiumCalculator(
				new AgeBasedPremiumCalculator(new GenderBasedPremiumCalculator(new BasicPremiumCalculator()))));
		Assert.assertEquals(5441.7, premiumCalc.calculatePremium(cust).getPremium(), 2);
	}
}
