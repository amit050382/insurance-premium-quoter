package com.insurance.quote.test;

public class BasicPremiumCalculator implements PremiumCalculator{

	private static final double BASIC_PREMIUM = 5000;
	
	@Override
	public Customer calculatePremium(Customer customer) {
		System.out.println("Basic premium calc....");
		customer.setPremium(BASIC_PREMIUM);
		return customer;
	}

}
