package com.insurance.quote.test;

import java.util.Iterator;
import java.util.Set;

public class HealthConditionBasedPremiumCalculator extends PremiumCalculatorDecorator {

	public HealthConditionBasedPremiumCalculator(PremiumCalculator premiumCalc) {
		super(premiumCalc);
	}
	
	@Override
	public Customer calculatePremium(Customer customer) {
		super.calculatePremium(customer);
		System.out.println("Health Condition Based premium calc...");
		Set<String> healthCond = customer.getHealthStatus().keySet();
		Iterator<String> healthItr = healthCond.iterator();
		String healthCondName = "";
		while(healthItr.hasNext()){
			healthCondName = healthItr.next();
			if(customer.getHealthStatus().get(healthCondName)){
				customer.setPremium(customer.getPremium() + (customer.getPremium() * 1/100));
			}
		}
		return customer;
	}
}
