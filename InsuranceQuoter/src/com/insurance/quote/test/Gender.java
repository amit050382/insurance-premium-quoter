package com.insurance.quote.test;

public enum Gender {
	MALE,
	FEMALE,
	OTHER
}
