package com.insurance.quote.test;

public interface PremiumCalculator {
	
	public Customer calculatePremium(Customer customer);

}
