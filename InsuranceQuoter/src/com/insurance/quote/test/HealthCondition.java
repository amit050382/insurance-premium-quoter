package com.insurance.quote.test;

public enum HealthCondition {
	HYPERTENSION,
	BLOOD_SUGAR,
	BLOOD_PRESSURE,
	OVERWEIGHT
}
