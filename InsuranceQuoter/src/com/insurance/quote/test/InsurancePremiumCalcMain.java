package com.insurance.quote.test;

import java.util.HashMap;
import java.util.Map;

public class InsurancePremiumCalcMain {

	public static void main(String[] args) {
		Customer cust = new Customer();
		cust.setName("Amit Kumar");
		cust.setAge(35);
		cust.setGender(Gender.MALE.toString());
		Map<String, Boolean> healthCond = new HashMap<>();
		healthCond.put(HealthCondition.HYPERTENSION.toString(), false);
		healthCond.put(HealthCondition.BLOOD_PRESSURE.toString(), false);
		healthCond.put(HealthCondition.BLOOD_SUGAR.toString(), false);
		healthCond.put(HealthCondition.OVERWEIGHT.toString(), false);
		
		cust.setHealthStatus(healthCond);
		
		Map<String, Boolean> habits = new HashMap<>();
		habits.put(Habits.BAD_HABIT_SMOKING.toString(), true);
		habits.put(Habits.BAD_HABIT_ALCOHOL.toString(), false);
		habits.put(Habits.GOOD_HABIT_DAILY_EXERCISE.toString(), true);
		habits.put(Habits.BAD_HABIT_DRUGS.toString(), false);
		
		cust.setHabits(habits);
		
		PremiumCalculator premiumCalc = new HabitsBasedPremiumCalculator(new HealthConditionBasedPremiumCalculator(
				new AgeBasedPremiumCalculator(new GenderBasedPremiumCalculator(new BasicPremiumCalculator()))));
		premiumCalc.calculatePremium(cust);
		System.out.println("PREMIUM --- " + cust.getPremium());
	}

}
