package com.insurance.quote.test;

import java.util.Iterator;
import java.util.Set;

public class HabitsBasedPremiumCalculator extends PremiumCalculatorDecorator {

	public HabitsBasedPremiumCalculator(PremiumCalculator premiumCalc) {
		super(premiumCalc);
	}
	
	@Override
	public Customer calculatePremium(Customer customer) {
		super.calculatePremium(customer);
		System.out.println("Habits Based premium calc...");
		Set<String> habits = customer.getHabits().keySet();
		Iterator<String> habitsItr = habits.iterator();
		String habitName = "";
		while(habitsItr.hasNext()){
			habitName = habitsItr.next();
			if(customer.getHabits().get(habitName) && habitName.contains("BAD_HABIT")){
				customer.setPremium(customer.getPremium() + (customer.getPremium() * 3/100));
			} else if(customer.getHabits().get(habitName) && habitName.contains("GOOD_HABIT")){
				customer.setPremium(customer.getPremium() - (customer.getPremium() * 3/100));
			}
		}
		return customer;
	}

}
