package com.insurance.quote.test;

public class PremiumCalculatorDecorator implements PremiumCalculator{

	protected PremiumCalculator premiumCalc;
	
	public PremiumCalculatorDecorator(PremiumCalculator premiumCalc) {
		this.premiumCalc = premiumCalc;
	}
	
	@Override
	public Customer calculatePremium(Customer customer) {
		return this.premiumCalc.calculatePremium(customer);
	}

}
